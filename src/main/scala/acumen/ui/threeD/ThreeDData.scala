package acumen
package ui
package threeD

import acumen.Errors._
import acumen.util.Conversions._
import scala.collection.mutable
import scala.swing._
import acumen.passes.extract_ha.Cond.False
import acumen.interpreters.enclosure.Rounding
import acumen.interpreters.enclosure.Interval
import acumen.interpreters.enclosure.Rounding
import java.util.Arrays
import java.io._

/* Get the 3D-visualization data */
class ThreeDData extends Publisher {
  /* Stores all the information for 3D-visualization */
  type _3DStore = mutable.Map[Int, _3DClass]
  /* Stores 3D-visualization information for a class */
  /* We store the CId and object number as a tuple,
			 * which is equal to (CId,objectNumber) */
  type _3DClass = mutable.Map[(CId, Int), List[_]]
  type ViewInfo = (Array[Double], Array[Double])
  /* Key of _3DData is the frame number, 
     key of _3DClass is the (CId,objectNumber) */
  var _3DData = mutable.Map[Int, _3DClass]()
  /* The number of 3D-objects */
  var objectCount = 1
  var frameNumber = 0
  /* Used for determine 3D-visualization play speed */
  var endTime = 0.0f
  /* Default settings to transform Acumen AST to generic data can be
					 * used later for constructing a primitive
       Example : GStr("Sphere") => "Sphere" */
  var _3DType = "Sphere"
  var _3DPosition = Array[Double](0.0, 0.0, 0.0)
  var _3DSize = Array[Double](0.2)
  var _3DColor = Array[Double](1.0, 1.0, 1.0)
  var _3DAngle = Array[Double](0.0, 0.0, 0.0)
  var _3DPath = ""
  var _3DText = ""
  var enclosure = false
  var first_lap = true
  /* Optional field to indicate transparent object or not */
  var _3DTexture = ""
  /* Camera's position and orientation*/
  var _3DView = mutable.ArrayBuffer[ViewInfo]()
  /*Enclosure*/
  var _3DPosition_delta = Array[Double](0.0, 0.0, 0.0)
  var _3DAngle_delta = Array[Double](0.0, 0.0, 0.0)
  /*OpacityBox*/
  var _3DObjSize = Array(Array[Double](0.0, 0.0, 0.0))
  var _Obj_counter = 0

  var _3DOpacityBoxSize = Array[Double](0.0, 0.0, 0.0)
  /*Scale the value*/
  val Scale_ang = 1.0 //1.0
  val Scale_pos = 1.0 //1.0
  val Scale_size = 1.0
  val OpacityBox = true
  /* CId of the object that contain _3DView info */
  var _3DViewID: CId = null
  /* Used for synchronize real time 3D-visualisation with real world time */
  protected[threeD] var timeStep = 0.0

  def reset() {
    /*Enclosure and OBJ values*/
    _3DObjSize = Array(Array(0.0, 0.0, 0.0))
    _Obj_counter = 0
    _3DOpacityBoxSize = Array[Double](0.0, 0.0, 0.0)
    first_lap = true
    enclosure = false
    _3DData.clear()
    _3DView.clear()
    frameNumber = 0
    _3DViewID = null
  }

  def isVectorOfNumbers(l: List[_]): Boolean = {
    var result = true
    for (x <- l)
      x match {
        case VLit(GInt(i))           =>
        case VLit(GDouble(i))        =>
        case VLit(e: GRealEnclosure) =>
        case _                       => result = false;
      }
    result
  }

  /* _3DType should be a String or a Integer */
  def extractType(value: Value[_]) {
    value match {
      case VLit(GStr(s)) => this._3DType = s
      case VLit(GInt(i)) => this._3DType = i.toString
      case VLit(e: GDiscreteEnclosure[_]) if e.isThin => this._3DType = e.range.head.toString
      case _ => throw _3DNameError(value)
    }
  }

  /* Check if the list's size is 3 and only contains numbers (int, double) */
  def checkVectorContent(l: List[_]): Boolean = {
    (l.size == 3) && isVectorOfNumbers(l)
  }

  /* _3D Position, color, angle should all be a vector with 3 numbers */
  def extractVector(value: Value[_], index: String) {
    def helper(value: Value[_]): Array[Double] = {
      value match {
        case VLit(GPattern(ls)) => helper(VVector(ls map VLit))
        case VVector(vs) =>
          if (checkVectorContent(vs)) {
            if (enclosure) {
              if (index.equals("position")) {
                val enclos_pos = extractIntervals(vs)
                var temp_array = new Array[Double](3)
                for (i <- 0 to 2) {
                  temp_array(i) = enclos_pos(i).loDouble + _3DPosition_delta(i) / 2
                }
                temp_array.toArray
              } else if (index.equals("angle")) {
                val enclos_val = extractIntervals(vs)
                var temp_array = new Array[Double](3)
                for (i <- 0 to 2) {
                  temp_array(i) = enclos_val(i).loDouble + _3DAngle_delta(i) / 2
                }
                temp_array.toArray
              } else if (index.equals("position_delta") || index.equals("angle_delta")) {
                val enclos_pos = extractIntervals(vs)
                var array_delta = new Array[Double](3)
                for (i <- 0 to 2) {
                  array_delta(i) = enclos_pos(i).hiDouble - enclos_pos(i).loDouble
                }
                array_delta.toArray
              } else { //for color
                val enclos_pos = extractIntervals(vs)
                var temp_array = new Array[Double](3)
                for (i <- 0 to 2) {
                  temp_array(i) = enclos_pos(i).loDouble
                }
                temp_array.toArray
              }
            } else {
              extractDoubles(vs).toArray
            }
          } else throw _3DVectorError(value, index)
        case _ => throw _3DVectorError(value, index);
      }
    }
    val temp = helper(value)
    index match {
      case "position"       => _3DPosition = temp
      case "position_delta" => _3DPosition_delta = temp
      case "color"          => _3DColor = temp
      case "angle"          => _3DAngle = temp
      case "angle_delta"    => _3DAngle_delta = temp
      case _                => throw ShouldNeverHappen()
    }
  }

  /* _3D OBJ path should be a string */
  def extractPath(value: Value[_]) {
    value match {
      case VLit(GStr(obj)) => _3DPath = obj
      case VLit(e: GDiscreteEnclosure[_]) if e.isThin => _3DPath = e.range.head.toString
      case _ => throw _3DNameError(value)
    }
  }

  /* _3D texture should be a string */
  def extractTexture(value: Value[_]) {
    value match {
      case VLit(GStr(tex)) => _3DTexture = tex
      case _               => throw _3DNameError(value)
    }
  }

  def extractText(value: Value[_]) {
    value match {
      case VLit(GStr(s))                  => _3DText = s
      case VLit(GInt(i))                  => _3DText = i.toString
      case VLit(GDouble(i))               => _3DText = i.toInt.toString
      case VLit(e: GDiscreteEnclosure[_]) => _3DText = e.range.head.toString //if e.isThin 
      case VLit(e: GRealEnclosure)        => _3DText = e.range.hiDouble.toInt.toString()
      case _                              => throw _3DNameError(value)
    }
  }

  /* _3D size should be either a vector or an number */
  def extractSize(value: Value[_]) {
    value match {
      case VLit(GPattern(ls)) => extractSize(VVector(ls map VLit))
      case VVector(vs) =>
        if (isVectorOfNumbers(vs)) {
          _3DSize = Array.ofDim[Double](vs.length);
          var i = 0;
          for (x <- vs) {
            x match {
              case VLit(GInt(x)) =>
                _3DSize(i) = x.toDouble
              case VLit(GDouble(x)) =>
                _3DSize(i) = x
              case VLit(e: GRealEnclosure) =>
                enclosure = true
                _3DSize(i) = e.range.hiDouble
            }
            i += 1
          }
        } else {
          _3DSize = Array[Double]()
          throw _3DSizeError(value)
        }
      case VLit(GInt(x)) =>
        _3DSize = Array(x.toDouble)
      case VLit(GDouble(x)) =>
        _3DSize = Array(x)
      case VLit(e: GRealEnclosure) =>
        enclosure = true;
        _3DSize = Array(e.range.hiDouble)
      case _ => throw _3DSizeError(value)
    }
    /* Check if the _3DSize is valid */
    this._3DType match {
      case "Sphere"   => if (_3DSize.length != 1) throw _3DSphereSizeError()
      case "Cylinder" => if (_3DSize.length != 2) throw _3DCylinderSizeError()
      case "Cone"     => if (_3DSize.length != 2) throw _3DConeSizeError()
      case "Box"      => if (_3DSize.length != 3) throw _3DBoxSizeError()
      /* 3D text's size should be a number */
      case _          => if (_3DSize.length != 1) throw _3DTextSizeError()
    }
  }

  def init3DClassStore(id: CId, _3DData: _3DStore, frameNumber: Int) = {
    val temp: _3DClass = mutable.Map[(CId, Int), List[_]]()
    _3DData += frameNumber -> temp
  }

  def addValuesTo3DClass(objectKey: (CId, Int), value: List[_],
                         _3DData: _3DStore, frameNumber: Int) = {
    if (_3DData(frameNumber).contains(objectKey))
      _3DData(frameNumber)(objectKey) = value
    else {
      _3DData(frameNumber) += objectKey -> value
    }
  }

  /* Add new information of each 3D-object to _3DStore */
  def addTo3DStore(id: CId, _3DData: _3DStore, value: List[Value[_]],
                   objectCount: Int, frameNumber: Int) {
    if (!_3DData.contains(frameNumber))
      init3DClassStore(id, _3DData, frameNumber)
    _Obj_counter = 0
    if (frameNumber == 1) { first_lap = false }
    var itemCount = 0
    for (i <- 0 until objectCount) {
      /* objectKey is used for specifying the 3D object */
      var objectKey = (id, i)
      val vector = value(i)
      vector match {
        case VVector(l) =>
          if (l.size != 5 && l.size != 6)
            throw _3DError(vector)
          else {
            extractType(l(0))
            extractSize(l(2))
            if (enclosure) {
              extractVector(l(1), "position_delta")
              extractVector(l(4), "angle_delta")

            } else {
              _3DPosition_delta = Array(0.0, 0.0, 0.0)
              _3DAngle_delta = Array(0.0, 0.0, 0.0)
            }
            extractVector(l(1), "position")
            extractVector(l(3), "color")
            extractVector(l(4), "angle")
            if (_3DType == "Text")
              extractText(l(5))
            else if (_3DType == "OBJ")
              extractPath(l(5))
            else if (l.size == 6)
              extractTexture(l(5))
          }
        case _ => throw ShouldNeverHappen()
      }
      if (OpacityBox && (enclosure && _3DType != "Text")) { //&& _3DType == "OBJ"
        addOpacityBox(itemCount, id)
        itemCount += 1
      } else if (first_lap && _3DType == "OBJ") { calcOpacitySize(_3DSize) }
      itemCount = addModels3DStore(itemCount, id)
    }
  }
  /* Create one/series of objects*/
  def addModels3DStore(itemCount: Int, id: CId): Int = {
    var Count = itemCount
    var temp_pos = Array(_3DPosition(0), _3DPosition(1), _3DPosition(2))
    var temp_ang = Array(_3DAngle(0), _3DAngle(1), _3DAngle(2))

    def addObject(Value: Double, Value_delta: Double, scale: Double, index: Int, length: Int): Double = {
      index match {
        case 0 =>
          Value - Value_delta / 2
        case 1 =>
          Value + Value_delta / 2
        case _ =>
          Value - (Value_delta / 2) + (Value_delta / length) * (index - 1)
      }
    }
    def calcLength(Delta: Double, Size: Double): Int = {
      Delta match {
        case 0 => 0
        case _ =>
          (Delta / Size).toInt.abs + 1
      }
    }
    //length
    var _x_length = 0
    var _y_length = 0
    var _z_length = 0

    var Temp_obj_size = Array(0.0, 0.0, 0.0)

    if (_3DType == "OBJ") {
      Temp_obj_size = Array(_3DObjSize(_Obj_counter)(0) * _3DSize(0), _3DObjSize(_Obj_counter)(1) * _3DSize(0), _3DObjSize(_Obj_counter)(2) * _3DSize(0))
      val sizeShell = calcSizeBox(Temp_obj_size)
      _x_length = calcLength(_3DPosition_delta(0), sizeShell(0) * Scale_pos)
      _y_length = calcLength(_3DPosition_delta(1), sizeShell(1) * Scale_pos)
      _z_length = calcLength(_3DPosition_delta(2), sizeShell(2) * Scale_pos)
    } else if (_3DType == "Box") {
      val sizeShell = calcSizeBox(_3DSize)
      _x_length = calcLength(_3DPosition_delta(0), sizeShell(0) * Scale_pos)
      _y_length = calcLength(_3DPosition_delta(1), sizeShell(1) * Scale_pos)
      _z_length = calcLength(_3DPosition_delta(2), sizeShell(2) * Scale_pos)
    } else if (_3DType != "Text") {
      _x_length = calcLength(_3DPosition_delta(0), _3DSize(0) * Scale_pos)
      _y_length = calcLength(_3DPosition_delta(1), _3DSize(0) * Scale_pos)
      _z_length = calcLength(_3DPosition_delta(2), _3DSize(0) * Scale_pos)
    }
    //angle
    var _x_ang = calcLength(_3DAngle_delta(0), Scale_ang)
    var _y_ang = calcLength(_3DAngle_delta(1), Scale_ang)
    var _z_ang = calcLength(_3DAngle_delta(2), Scale_ang)

    if (_3DType == "Text") {
      _x_ang = 0
      _y_ang = 0
      _z_ang = 0
    }

    for (_x <- 0 to _x_length) {
      temp_pos(0) = addObject(_3DPosition(0), _3DPosition_delta(0), Scale_pos, _x, _x_length)
      for (_y <- 0 to _y_length) {
        temp_pos(1) = addObject(_3DPosition(1), _3DPosition_delta(1), Scale_pos, _y, _y_length)
        for (_z <- 0 to _z_length) {
          temp_pos(2) = addObject(_3DPosition(2), _3DPosition_delta(2), Scale_pos, _z, _z_length)
          for (_a <- 0 to _x_ang) {
            temp_ang(0) = addObject(_3DAngle(0), _3DAngle_delta(0), Scale_ang, _a, _x_ang)
            for (_b <- 0 to _y_ang) {
              temp_ang(1) = addObject(_3DAngle(1), _3DAngle_delta(1), Scale_ang, _b, _y_ang)
              for (_c <- 0 to _z_ang) {
                temp_ang(2) = addObject(_3DAngle(2), _3DAngle_delta(2), Scale_ang, _c, _z_ang)
                val valueList =
                  if (_3DType == "Text")
                    List(_3DType, temp_pos.clone(), _3DSize, _3DColor, temp_ang.clone(), _3DText)
                  else if (_3DType == "OBJ") {
                    List(_3DType, temp_pos.clone(), _3DSize, _3DColor, temp_ang.clone(), _3DPath)
                    //                    List("Box", temp_pos.clone(), Temp_obj_size, _3DColor, temp_ang.clone())
                  } else if (_3DTexture == "transparent")
                    List(_3DType, temp_pos.clone(), _3DSize, _3DColor, temp_ang.clone(), _3DTexture)
                  else
                    List(_3DType, temp_pos.clone(), _3DSize, _3DColor, temp_ang.clone())
                addValuesTo3DClass((id, Count), valueList, _3DData, frameNumber)
                Count += 1
              }
            }
          }
        }
      }
    }
    if (_3DType == "OBJ") { _Obj_counter += 1 }
    Count
  }

  /*Adds OpacityBox to 3DStore*/
  def addOpacityBox(itemCount: Int, id: CId) {
    calcOpacitySize(_3DSize)
    var min_length = _3DOpacityBoxSize(0)
    if (min_length > _3DOpacityBoxSize(1)) { min_length = _3DOpacityBoxSize(1) }
    if (min_length > _3DOpacityBoxSize(2)) { min_length = _3DOpacityBoxSize(2) }
    val Errror_margin = min_length * 0.03
    val _tranColor = Array(0.5, 0.5, 0.5, 0)
    val valueList = List("Box", _3DPosition, Array(_3DOpacityBoxSize(0) + Errror_margin, _3DOpacityBoxSize(1) + Errror_margin, _3DOpacityBoxSize(2) + Errror_margin), _tranColor, Array(0.0, 0.0, 0.0))
    addValuesTo3DClass((id, itemCount), valueList, _3DData, frameNumber)
    _3DTexture = ""
  }

  def calcSizeBox(size: Array[Double]): Array[Double] = {
    def MaxAngleSide(index: Int, _a: Double, _b: Double): Double = {
      def caleAngleBoxArea(ang: Double, _a: Double, _b: Double): Double = {
        (caleAngleBox(ang, _a, _b) * caleAngleBox(ang, _b, _a)).abs
      }
      var Angle = Array(_3DAngle(index) - _3DAngle_delta(index) / 2, _3DAngle(index), _3DAngle(index) + _3DAngle_delta(index) / 2)
      var MaxArea = Array(caleAngleBoxArea(_3DAngle(index) - _3DAngle_delta(index) / 2, _a, _b),
        caleAngleBoxArea(_3DAngle(index), _a, _b),
        caleAngleBoxArea(_3DAngle(index) + _3DAngle_delta(index) / 2, _a, _b))
      for (i <- 0 to 1) {
        for (j <- i to 1) {
          if (MaxArea(i + 1) > MaxArea(i)) {
            val temp = MaxArea(i + 1)
            MaxArea(i + 1) = MaxArea(i)
            MaxArea(i) = temp
            //*************
            val temp_0 = Angle(i + 1)
            Angle(i + 1) = Angle(i)
            Angle(i) = temp_0
          }
        }
      }
      Angle(0)
    }

    def caleAngleBox(ang: Double, _a: Double, _b: Double): Double = {
      _a * (Math.cos(ang)).abs + _b * (Math.sin(ang)).abs
    }
    val maxSize = Math.sqrt(size(0) * size(0) + size(1) * size(1) + size(2) * size(2))
    var sizeShell = size.clone()
    if (_3DAngle(0).abs > 0 || _3DAngle_delta(0).abs > 0) { sizeShell(2) = maxSize; sizeShell(1) = maxSize }
    if (_3DAngle(1).abs > 0 || _3DAngle_delta(1).abs > 0) { sizeShell(2) = maxSize; sizeShell(0) = maxSize }
    if (_3DAngle(2).abs > 0 || _3DAngle_delta(2).abs > 0) {
      if ((_3DAngle(1) == 0 || _3DAngle_delta(1) == 0) && (_3DAngle(0) == 0 || _3DAngle_delta(0) == 0)) { val ang = MaxAngleSide(2, size(0), size(1)); sizeShell(1) = caleAngleBox(ang, size(1), size(0)); sizeShell(0) = caleAngleBox(ang, size(0), size(1)) }
      else { sizeShell(1) = maxSize; sizeShell(0) = maxSize }
    }
    sizeShell
  }

  def calcOpacitySize(objSize: Array[Double]) {
    /* Calculate the size of the OpacityBox (space diagonal formula)  */
    def calcSizeBoxRot(size: Array[Double]): Array[Double] = {
      val maxSize = Math.sqrt(size(0) * size(0) + size(1) * size(1) + size(2) * size(2))
      var sizeShell = size.clone()

      if (_3DAngle(0).abs > 0 || _3DAngle_delta(0).abs > 0) { sizeShell(2) = maxSize; sizeShell(1) = maxSize }
      if (_3DAngle(1).abs > 0 || _3DAngle_delta(1).abs > 0) { sizeShell(2) = maxSize; sizeShell(0) = maxSize }
      if (_3DAngle(2).abs > 0 || _3DAngle_delta(2).abs > 0) { sizeShell(1) = maxSize; sizeShell(0) = maxSize }
      sizeShell
    }

    /* Check if the _3DSize is valid */
    this._3DType match {

      case "Sphere" =>
        val basSize = objSize(0) * 2
        _3DOpacityBoxSize = Array(basSize + _3DPosition_delta(0), basSize + _3DPosition_delta(1), basSize + _3DPosition_delta(2))
      case "Cylinder" =>
        val basSize = objSize(0) * 2
        _3DOpacityBoxSize = Array(basSize + _3DPosition_delta(0), basSize + _3DPosition_delta(1), basSize + _3DPosition_delta(2))
      case "Cone" =>
        val basSize = objSize(0) * 2
        _3DOpacityBoxSize = Array(basSize + _3DPosition_delta(0), basSize + _3DPosition_delta(1), basSize + _3DPosition_delta(2))
        _3DPosition = Array(_3DPosition(0), _3DPosition(1) + 0.35, _3DPosition(2))
      case "Box" =>
        var sizeShell = calcSizeBox(objSize)
        _3DOpacityBoxSize = Array(sizeShell(0) + _3DPosition_delta(0),
          sizeShell(1) + _3DPosition_delta(1),
          sizeShell(2) + _3DPosition_delta(2))
      case "Text" =>
      case "OBJ" =>

        /**/
        if (first_lap) {
          val o = new OpasityBoxSize
          val temp = o.loadObjGetBoundingBox(_3DPath)
          if (_Obj_counter == 0) {
            _3DObjSize(_Obj_counter) = temp
          } else {
            _3DObjSize = _3DObjSize :+ temp
          }
        }
        val sizeShell = calcSizeBox(Array(_3DObjSize(_Obj_counter)(0) * _3DSize(0), _3DObjSize(_Obj_counter)(1) * _3DSize(0), _3DObjSize(_Obj_counter)(2) * _3DSize(0)))
        _3DOpacityBoxSize = Array(
          sizeShell(0) + _3DPosition_delta(0), //0
          sizeShell(1) + _3DPosition_delta(1), //1
          sizeShell(2) + _3DPosition_delta(2)) //2
      case _ => println("Missing:" + _3DType); throw _3DTextSizeError()
    }
  }

  /* Look for endTime in "Main" class */
  def lookUpEndTime(id: CId, o: GObject) {
    if (id.equals(new CId(List(0))))
      for ((name, value) <- o)
        if (name.x == "endTime")
          this.endTime = extractDouble(value).toFloat
  }

  /* Look for 3D camera's position and orientation in "Main" class */
  def lookUpViewInfo(id: CId, o: GObject) {
    //  CId(List()) is the main class
    if (id.equals(new CId(List())))
      for ((name, value) <- o)
        if (name.x == "_3DView")
          value match {
            case VVector(l) =>
              if (l.size > 0) {
                _3DView += new Tuple2(extractDoubles(l(0)).toArray,
                  extractDoubles(l(1)).toArray)
                if (_3DViewID != id && _3DViewID != null)
                  throw _3DViewDuplicateError(id, _3DViewID)
                _3DViewID = id
              }
            case VLit(_) => // initialization is empty
              if (_3DViewID != id && _3DViewID != null)
                throw _3DViewDuplicateError(id, _3DViewID)
              _3DViewID = id
            case _ => throw _3DError(value)
          }
  }

  /* Add _3D information of every class to _3DStore */
  def get3DData(s: GStore): Unit = {
    for ((id, o) <- s) {
      lookUpEndTime(id, o)
      lookUpViewInfo(id, o)
      /* Look for variable named _3D */
      for ((name, value) <- o) {
        if (name.x == "_3D") {
          value match {
            case VVector(l) =>
              if (l.size == 0) {} //
              else
                l(0) match {
                  /* If only one object, _3D will start with a string or an int,
							 *  example:  _3D = ("Sphere",...,...,..),
							 *            _3D = (2,...,...,..)
							 */
                  case VLit(_) =>
                    addTo3DStore(id, _3DData, List(value), 1, frameNumber)
                  /*If it contains multiple objects, _3D will start with a vector,
							 *  example: _3D = (" Sphere ", ( ), ( )...,
							 *                  "Sphere",(),()...)..)
							 */
                  case VVector(some) =>
                    addTo3DStore(id, _3DData, l, l.size, frameNumber)
                  case _ => throw _3DError(value)
                }
            case _ => throw _3DError(value)
          }
        }
      }
    }
    if (!_3DData.contains(frameNumber))
      _3DData += frameNumber -> null
    if (_3DData(frameNumber) != null && !App.ui.views.threeDViewSelected)
      App.ui.views.selectThreeDView()
    frameNumber += 1
  }

  /* Check if model source contains any _3D variable declarations. */
  def modelContains3D(): Boolean = {
    var _3DIsEmpty = false
    if (_3DData.nonEmpty) {
      for ((frameNum, objects) <- _3DData)
        if (objects != null)
          _3DIsEmpty = true
    } else _3DIsEmpty = false
    _3DIsEmpty
  }
}
