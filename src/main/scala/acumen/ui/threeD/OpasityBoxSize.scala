package acumen.ui.threeD
import com.threed.jpct._
import acumen.Errors._
import java.io._
import acumen.ui.Files

class OpasityBoxSize {
  def loadObjGetBoundingBox(path: String, size: Double): Array[Double] = {
    val _3DBasePath = Files._3DDir.getAbsolutePath
    //read in the geometry information from the data file
    val _3DFolderPath = (_3DBasePath + File.separator + path).split("\\.")(0)
    val _3DFolder = new File(_3DFolderPath) // the folder of the objects' files
    // load all the texture files
    val objectFiles =
      if (_3DFolder.canExecute) _3DFolder.listFiles()
      else throw _3DLoadFileError(path)
    val objFile = objectFiles.filter(file => file.getName.endsWith(".3ds") || file.getName.endsWith(".obj"))
    val textureFiles = objectFiles.filter(file => file.getName.endsWith(".png") || file.getName.endsWith(".jpg"))
    val mtlFile = objectFiles.filter(file => file.getName.endsWith(".mtl"))
    // make sure there's only one object file
    if (objFile.isEmpty) throw _3DLoadFileError("Can not find object file for " + path)
    else if (objFile.length > 1) throw _3DLoadFileError("Duplicate object files for " + path)
    // load OBJ file
    val resultObject =
      if (objFile.head.getName.endsWith(".obj")) {
        // make sure there's only one material file
        if (mtlFile.isEmpty) throw _3DLoadFileError("Can not find material file for " + path)
        else if (mtlFile.length > 1) throw _3DLoadFileError("Duplicate material files for " + path)
        else {
          for (texture <- textureFiles) {
            if (!TextureManager.getInstance().containsTexture(texture.getName))
              TextureManager.getInstance().addTexture(texture.getName, new Texture(texture.toString))
          }
        }
        Object3D.mergeAll(Loader.loadOBJ(objFile.head.toString, mtlFile.head.toString, size.toFloat))
      } // load 3ds file
      else {
        for (texture <- textureFiles) {
          if (!TextureManager.getInstance().containsTexture(texture.getName))
            TextureManager.getInstance().addTexture(texture.getName, new Texture(texture.toString))
        }
        Object3D.mergeAll(Loader.load3DS(objFile.head.toString, size.toFloat))
      }
    val f = resultObject.getMesh.getBoundingBox
    resultObject.clearObject()
    //resultObject
    val ObjScale = 0.0103
    val _x = (f(1) - f(0)) * ObjScale
    val _y = (f(3) - f(2)) * ObjScale
    val _z = (f(5) - f(4)) * ObjScale
    Array[Double](_x.toDouble, _z.toDouble, _y.toDouble)
  }
}