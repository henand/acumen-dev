/**
* Actuators for moving the bat, which also keeps track of the energy spent.
**/

model BatActuator (n,p1,tablerow,tablecolum,maxE) =
initially
  p        =  p1,
  p'       =  (0,0,0),
  p''      =  (0,0,0),
  angle    =  (0,0,0),
  energy   =  0,
  energy'  =  0,
  m        =  0.1,      // Mass of the bat
  F        =  0,
  v        =  (0,0,0),
  offset   =  0,
  // Visualize energy spent
  eb       =  create Cylinder((0,0,0),tablerow,tablecolum),
  link1    =  create Cylinder((0,0,0),tablerow,tablecolum),
  link2    =  create Cylinder((0,0,0),tablerow,tablecolum),
  L1       =  0.4,      // Length of link1 is fixed
  baseP    =  (2.4*(-1)^n,0,0.3),
  unit     =  (0,0,0)  // Unit vector from baseP to P
always
  F = m * norm(p''),
  if norm(p') > 5 then  // ???Saturate??? speeds over 5
    p' + = p'/norm(p') * 5 
  noelse,
  energy' = F * norm(p'), // Simplistic model of power consumed
  // Visualize arm
  unit    = (p - baseP)/norm(p - baseP),
  link1.q = baseP,
  link1.p = baseP + L1 * unit,
  link1.r = 0.1,
  link2.q = link1.p,
  link2.p = p +  ((-1)^n * 0.05,0,0),
  link2.r = 0.03,
  // Energy bar visualization
  offset  = (energy/maxE)*1.5,
  eb.p    = ((-1)^n * 1.5, -0.8, -0.05),
  eb.q    = ((-1)^n * 1.5 + (-1)^(n+1) * offset, -0.8, -0.05),
  eb.r    = 0.02,
  eb.col  = (0.2,1,0.2)
